name := "chess"

version := "0.1"

scalaVersion := "2.13.3"

libraryDependencies += "org.scalactic" %% "scalactic" % "3.2.0"
libraryDependencies += "org.scalatest" %% "scalatest" % "3.2.0" % "test"
libraryDependencies += "org.scalatestplus" %% "easymock-3-2" % "3.2.2.0" % "test"

scalacOptions ++= Seq(
  "-language:postfixOps",
  "-deprecation", "-feature"
)
