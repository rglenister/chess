package chess

import chess.PieceType._
import chess.PieceColor._
import chess.BoardSide._

import org.scalatest.flatspec.AnyFlatSpec

/**
 * Tests the CastlingMetadata
 */
class CastlingMetadataSpec extends AnyFlatSpec {

  "The CastlingMetadata" should "should be correct for white on the kingside" in {
    val castlingMetadata = CastlingMetadata.get(White)(Kingside)
    assert(castlingMetadata.kingFromSquare == 4)
    assert(castlingMetadata.kingToSquare == 6)
    assert(castlingMetadata.rookFromSquare == 7)
    assert(castlingMetadata.rookToSquare == 5)
    assert(castlingMetadata.squaresBetweenKingAndRook == List(5, 6))
  }
  
  it should "be correct for white on the queenside" in {
    val castlingMetadata = CastlingMetadata.get(White)(Queenside)
    assert(castlingMetadata.kingFromSquare == 4)
    assert(castlingMetadata.kingToSquare == 2)
    assert(castlingMetadata.rookFromSquare == 0)
    assert(castlingMetadata.rookToSquare == 3)
    assert(castlingMetadata.squaresBetweenKingAndRook == List(3, 2, 1))
  }
  
  it should "be correct for black on the kingside" in {
    val castlingMetadata = CastlingMetadata.get(Black)(Kingside)
    assert(castlingMetadata.kingFromSquare == 60)
    assert(castlingMetadata.kingToSquare == 62)
    assert(castlingMetadata.rookFromSquare == 63)
    assert(castlingMetadata.rookToSquare == 61)
    assert(castlingMetadata.squaresBetweenKingAndRook == List(61, 62))
  }
  
  it should "be correct for black on the queenside" in {
    val castlingMetadata = CastlingMetadata.get(Black)(Queenside)
    assert(castlingMetadata.kingFromSquare == 60)
    assert(castlingMetadata.kingToSquare == 58)
    assert(castlingMetadata.rookFromSquare == 56)
    assert(castlingMetadata.rookToSquare == 59)
    assert(castlingMetadata.squaresBetweenKingAndRook == List(59, 58, 57))
  }
}


/**
 * Tests the CastlingRights
 */
class CastlingRightsSpec extends AnyFlatSpec {

  "The CastlingRights" should "be all false for an empty position" in {
    val squareToPieceMap: Map[Int, Piece] = Map()
    val castlingRights = CastlingRights.next(squareToPieceMap)
    assert(castlingRights(White).canCastle(Kingside) == false)
    assert(castlingRights(White).canCastle(Queenside) == false)
    assert(castlingRights(Black).canCastle(Kingside) == false)
    assert(castlingRights(Black).canCastle(Queenside) == false)
  }

  it should "not allow castling rights to be regained if the kings and rooks move back to their home squares" in {
    val castlingRights1 = CastlingRights.next(Map())
    assert(castlingRights1(White).canCastle(Kingside) == false)
    assert(castlingRights1(White).canCastle(Queenside) == false)
    assert(castlingRights1(Black).canCastle(Kingside) == false)
    assert(castlingRights1(Black).canCastle(Queenside) == false)

    val squareToPieceMap: Map[Int, Piece] = Map(4 -> Piece(King, White), 0 -> Piece(Rook, White), 7 -> Piece(Rook, White),
                                                60 -> Piece(King, Black), 56 -> Piece(Rook, Black), 63 -> Piece(Rook, Black))
    val castlingRights2 = CastlingRights.next(squareToPieceMap, castlingRights1)
    assert(castlingRights2(White).canCastle(Kingside) == false)
    assert(castlingRights2(White).canCastle(Queenside) == false)
    assert(castlingRights2(Black).canCastle(Kingside) == false)
    assert(castlingRights2(Black).canCastle(Queenside) == false)
  }
  
  it should "be all true when all kings and rooks are on their home squares" in {
    val squareToPieceMap: Map[Int, Piece] = Map(4 -> Piece(King, White), 0 -> Piece(Rook, White), 7 -> Piece(Rook, White),
                                                60 -> Piece(King, Black), 56 -> Piece(Rook, Black), 63 -> Piece(Rook, Black))
    val castlingRights = CastlingRights.next(squareToPieceMap)
    assert(castlingRights(White).canCastle(Kingside) == true)
    assert(castlingRights(White).canCastle(Queenside) == true)
    assert(castlingRights(Black).canCastle(Kingside) == true)
    assert(castlingRights(Black).canCastle(Queenside) == true)
  }

  it should "not allow white to castle kingside if the kingside rook is removed" in {
    val squareToPieceMap: Map[Int, Piece] = Map(4 -> Piece(King, White), 0 -> Piece(Rook, White),
                                                60 -> Piece(King, Black), 56 -> Piece(Rook, Black), 63 -> Piece(Rook, Black))
    val castlingRights = CastlingRights.next(squareToPieceMap)
    assert(castlingRights(White).canCastle(Kingside) == false)
    assert(castlingRights(White).canCastle(Queenside) == true)
    assert(castlingRights(Black).canCastle(Kingside) == true)
    assert(castlingRights(Black).canCastle(Queenside) == true)
  }

  it should "not allow black to castle kingside if the kingside rook is replaced by another piece" in {
    val squareToPieceMap: Map[Int, Piece] = Map(4 -> Piece(King, White), 0 -> Piece(Rook, White), 7 -> Piece(Rook, White),
                                                60 -> Piece(King, Black), 56 -> Piece(Rook, Black), 63 -> Piece(Knight, Black))
    val castlingRights = CastlingRights.next(squareToPieceMap)
    assert(castlingRights(White).canCastle(Kingside) == true)
    assert(castlingRights(White).canCastle(Queenside) == true)
    assert(castlingRights(Black).canCastle(Kingside) == false)
    assert(castlingRights(Black).canCastle(Queenside) == true)
  }

  it should "not allow white to castle queenside if the queenside rook is removed" in {
    val squareToPieceMap: Map[Int, Piece] = Map(4 -> Piece(King, White), 7 -> Piece(Rook, White),
                                                60 -> Piece(King, Black), 56 -> Piece(Rook, Black), 63 -> Piece(Rook, Black))
    val castlingRights = CastlingRights.next(squareToPieceMap)
    assert(castlingRights(White).canCastle(Kingside) == true)
    assert(castlingRights(White).canCastle(Queenside) == false)
    assert(castlingRights(Black).canCastle(Kingside) == true)
    assert(castlingRights(Black).canCastle(Queenside) == true)
  }

  it should "not allow black to castle queenside if the queenside rook is removed" in {
    val squareToPieceMap: Map[Int, Piece] = Map(4 -> Piece(King, White), 0 -> Piece(Rook, White), 7 -> Piece(Rook, White),
                                                60 -> Piece(King, Black), 63 -> Piece(Rook, Black))
    val castlingRights = CastlingRights.next(squareToPieceMap)
    assert(castlingRights(White).canCastle(Kingside) == true)
    assert(castlingRights(White).canCastle(Queenside) == true)
    assert(castlingRights(Black).canCastle(Kingside) == true)
    assert(castlingRights(Black).canCastle(Queenside) == false)
  }

  it should "not allow white to castle on either side if the king is removed" in {
    val squareToPieceMap: Map[Int, Piece] = Map(7 -> Piece(Rook, White), 0 -> Piece(Rook, White),
                                                60 -> Piece(King, Black), 56 -> Piece(Rook, Black), 63 -> Piece(Rook, Black))
    val castlingRights = CastlingRights.next(squareToPieceMap)
    assert(castlingRights(White).canCastle(Kingside) == false)
    assert(castlingRights(White).canCastle(Queenside) == false)
    assert(castlingRights(Black).canCastle(Kingside) == true)
    assert(castlingRights(Black).canCastle(Queenside) == true)
  }

  it should "not allow black to castle on either side if the king is replaced with another piece" in {
    val squareToPieceMap: Map[Int, Piece] = Map(4 -> Piece(King, White), 0 -> Piece(Rook, White), 7 -> Piece(Rook, White),
                                                60 -> Piece(Bishop, White), 56 -> Piece(Rook, Black), 63 -> Piece(Rook, Black))
    val castlingRights = CastlingRights.next(squareToPieceMap)
    assert(castlingRights(White).canCastle(Kingside) == true)
    assert(castlingRights(White).canCastle(Queenside) == true)
    assert(castlingRights(Black).canCastle(Kingside) == false)
    assert(castlingRights(Black).canCastle(Queenside) == false)
  }

  it should "be all false when initialized from an empty FEN" in {
    val castlingRights = CastlingRights.create("-")
    assert(castlingRights(White).canCastle(Kingside) == false)
    assert(castlingRights(White).canCastle(Queenside) == false)
    assert(castlingRights(Black).canCastle(Kingside) == false)
    assert(castlingRights(Black).canCastle(Queenside) == false)
  }

  it should "be all true when initialized from a FEN that includes full castling rights for both sides" in {
    val castlingRights = CastlingRights.create("KQkq")
    assert(castlingRights(White).canCastle(Kingside) == true)
    assert(castlingRights(White).canCastle(Queenside) == true)
    assert(castlingRights(Black).canCastle(Kingside) == true)
    assert(castlingRights(Black).canCastle(Queenside) == true)
  }
}  
  