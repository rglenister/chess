package chess

import org.scalatest.flatspec.AnyFlatSpec
import org.easymock.EasyMock
import chess.PieceType._
import chess.PieceColor._
import chess.BoardSide._
import Board.{algebraicToSquareIndex => aToI}
import org.scalatestplus.easymock.EasyMockSugar


class PositionSpec extends AnyFlatSpec with EasyMockSugar {
 
  "The Position" should "know the color of the side to move and the opposing side" in {
    val whiteToMove = new TestPosition(Map(), White)
    assert(whiteToMove.sideToMove == White)
    assert(whiteToMove.opposingSide == Black)
    
    val blackToMove = new TestPosition(Map(), Black)
    assert(blackToMove.sideToMove == Black)
    assert(blackToMove.opposingSide == White)
  }

  it should "know if a square is empty" in {
    assert((new TestPosition(Map(), White) isEmpty 0)  == true)
    assert((new TestPosition(Map(0 -> Piece(Pawn, White)), White) isEmpty 0) == false)
  }

  it should "know what piece is on a square" in {
    assert((new TestPosition(Map(0 -> Piece(Pawn, White)), White) squareToPieceMap 0) == Piece(Pawn, White))
  }

  it should "know what square a piece is on" in {
    val squareToPieceMap = Map(8 -> Piece(Pawn, White), 9 -> Piece(Pawn, White), 10 -> Piece(Bishop, Black))
    val pieceToSquaresMap = new TestPosition(squareToPieceMap, White).pieceToSquaresMap
    assert(pieceToSquaresMap(Piece(Pawn, White)) == List(8, 9))
    assert(pieceToSquaresMap(Piece(Bishop, Black)) == List(10))
  }

  it should "know the location of the kings" in {
    val position = new TestPosition(Map(7 -> Piece(King, White), 8 -> Piece(King, Black)), White)
    assert(position.getKingSquare(White) == 7)
    assert(position.getKingSquare(Black) == 8)
  }

  it should "know the castling rights map" in {
    val whiteCastlingRights = mock[CastlingRights]
    val blackCastlingRights = mock[CastlingRights]
    val castlingRightsMap = Map(White -> whiteCastlingRights, Black -> blackCastlingRights)
    val position = new TestPosition(Map(), White, castlingRightsMap)
    assert(position.castlingRightsMap == (castlingRightsMap))
  }

  it should "give the correct castling rights" in {
    val castlingRightsMap = Map(White -> mock[CastlingRights], Black -> mock[CastlingRights])
    for (color <- PieceColor.values; boardSide <- BoardSide.values) {
      EasyMock.expect(castlingRightsMap(color).canCastle(boardSide).andReturn(false))
      EasyMock.expect(castlingRightsMap(color).canCastle(boardSide).andReturn(true))
    }
    val position = new TestPosition(Map(), White, castlingRightsMap)
    EasyMock.replay(castlingRightsMap(White))
    EasyMock.replay(castlingRightsMap(Black))

    for (color <- PieceColor.values; boardSide <- BoardSide.values) {
      assert(position.canCastle(color, boardSide) == false)
      assert(position.canCastle(color, boardSide) == true)
    }
  }
  
  it should "know the en passant square" in {
    val position1 = new TestPosition(Map(), White, Map(), Some(3))
    assert(position1.enPassantSquare.contains(3))

    val position2 = new TestPosition(Map(), White, Map(), None)
    assert(position2.enPassantSquare.isEmpty)
  }

  class TestPosition(
    val squareToPieceMap: Map[Int, Piece],
    val sideToMove: PieceColor.Value,
    val castlingRightsMap: Map[PieceColor.Value, CastlingRights] = Map(),
    val enPassantSquare: Option[Int] = None,
    val fiftyMoveRuleCount: Int = 0,
    val fullMoveNumber: Int = 1,
    val previousPositions: List[Position] = Nil) extends Position {
  }
}


class GamePositionSpec extends AnyFlatSpec {
 
  "The GamePosition" should "be ready for a new game" in {
    val gamePosition = GamePosition()
    assert(gamePosition.squareToPieceMap == Board.startingPosition)
    assert(gamePosition.sideToMove == White)
    assert(gamePosition.castlingRightsMap(White) == CastlingRights(false, Map(Kingside -> false, Queenside -> false)))
    assert(gamePosition.castlingRightsMap(Black) == CastlingRights(false, Map(Kingside -> false, Queenside -> false)))
    assert(gamePosition.fiftyMoveRuleCount == 0)
    assert(gamePosition.fullMoveNumber == 1)
    assert(gamePosition.repetitionOfPositionCount == 0)
    assert(gamePosition.enPassantSquare.isEmpty)
    assert(gamePosition.isCheck == false)
    assert(gamePosition.previousPositions == Nil)
    assert(gamePosition.moveList.length == 20)
  }

  it should "checkmate when the side to move cannot get out of check" in {
    val gamePosition1 = GamePosition()
    val gamePosition2 = GamePosition(gamePosition1, getMove(gamePosition1, "f2 f3")).get
    val gamePosition3 = GamePosition(gamePosition2, getMove(gamePosition2, "e7 e5")).get
    val gamePosition4 = GamePosition(gamePosition3, getMove(gamePosition3, "g2 g4")).get
    val gamePosition5 = GamePosition(gamePosition4, getMove(gamePosition4, "d8 h4")).get
    assert(gamePosition5.isCheck == true)
    assert(gamePosition5.moveList.isEmpty)
      assert(gamePosition5.gameStatus == GameStatus.Checkmate)
  }

  it should "stalemate when the side to move cannot move but is not in check" in {
    val squareToPieceMap = Map(aToI("g6") -> Piece(Queen, White), aToI("f7") -> Piece(King, White), aToI("h8") -> Piece(King, Black))
    val gamePosition = GamePosition(squareToPieceMap, Black, CastlingRights.create(squareToPieceMap))
    assert(gamePosition.isCheck == false)
    assert(gamePosition.moveList.isEmpty)
    assert(gamePosition.gameStatus == GameStatus.Stalemate)
  }

  private def getMove(gamePosition: GamePosition, move: String): Move = {
    move.split("\\W") match {
      case Array(from, to) => gamePosition.moveList.find(m => m.fromSquare==aToI(from) && m.toSquare==aToI(to)) get 
    }
  }
}