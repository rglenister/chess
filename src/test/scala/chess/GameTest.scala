package chess

import org.scalatest.flatspec.AnyFlatSpec
import chess.PieceType._
import chess.PieceColor._
import Board.{algebraicToSquareIndex => aToI}


class GameSpec extends AnyFlatSpec {
 
  "The Game" should "be ready for a new game" in {
    assert(new Game().currentPosition == GamePosition())
  }
  
  it should "add a move to its move list after making a valid move" in {
    assert(new Game().makeMove(aToI("e2"), aToI("e4")).get.moveMap.size  == 1)
  }
  
  it should "be able to go back to the previous position or forwards to the next position" in {
    val game1 = new Game
    val game2 = game1.makeMove(aToI("e2"), aToI("e4")).get
    assert(game2.currentPosition != GamePosition())
    
    val game3 = game2.previousPosition.get
    assert(game3.currentPosition == game1.currentPosition)
    
    val game4 = game3.nextPosition.get
    assert(game4.currentPosition == game2.currentPosition)
  }
  
  it should "truncate the move list if a move is made from a previous position" in {
    val game1 = new Game
    val game2 = game1.makeMove(aToI("e2"), aToI("e4")).get
    val game3 = game2.previousPosition.get
    assert(game3.moveMap.size == 1)
    
    val game4 = game3.makeMove(aToI("d2"), aToI("d4")).get
    assert(game4.moveMap.size == 1)
    assert(game4.currentPosition != game2.currentPosition)
  }
  
  it should "select the promotion move with the correct promotion piece" in {
    val squareToPieceMap = Map(aToI("e1") -> Piece(King, White), aToI("e8") -> Piece(King, Black), aToI("b7") -> Piece(Pawn, White))
    val position = GamePosition(squareToPieceMap, White, CastlingRights.create(squareToPieceMap))
    val game1 = new Game(position)
    val game2 = game1.makeMove(aToI("b7"), aToI("b8"), Some(Bishop)).get
    assert(game2.currentPosition.squareToPieceMap(aToI("b8")) == Piece(Bishop, White))
  }
  
  it should "return None if an illegal move is attempted" in {
    assert(new Game().makeMove(aToI("e2"), aToI("d3")).isEmpty)
  }
  
  it should "provide details of each player" in {
    assert(new Game().players(White) == Player("White Player"))
    assert(new Game().players(Black) == Player("Black Player"))
  }

  it should "allow the details of each player to be updated" in {
    assert(new Game().setPlayer(White, Player("New White Player")).players(White) == Player("New White Player"))
    assert(new Game().setPlayer(Black, Player("New Black Player")).players(Black) == Player("New Black Player"))
  }
}
