package chess

import org.scalatest.flatspec.AnyFlatSpec


class BoardSpec extends AnyFlatSpec {
 
  "The Board" should "convert algebraic notation to square index" in {
		Board.algebraicToSquareIndex("a1") == 0
		Board.algebraicToSquareIndex("h8") == 63
		Board.algebraicToSquareIndex("e8") == 60
  }
}